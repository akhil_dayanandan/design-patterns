<?php
/*
* Example for Adapter pattern
*/
interface iEnemyAttacker {
	public function fireWeapon();
    public function driveForward();
    public function assignDriver($driver);
}

class EnemyTank implements iEnemyAttacker
{
	public function fireWeapon()
	{
		echo "Enemy tank fired shots";
	}

	public function driveForward()
	{
		echo "Enemy Tank is moving forward";
	}

	public function assignDriver($driver)
	{
		echo "Driver $driver is driving the enemy tank ";
	}
}

// Adaptee Class

class EnemyRobot {
	public function smashWithHands()
	{
		echo "Enemy robot is causing damage with its hands";
	}

	public function walkForward()
	{
		echo "Enemy robot walks forward";
	}

	public function reactToHuman($driver)
	{
		echo "Enemy robot tramps on $driver";
	}
}

// Adapter class
class EnemyRobotAttacker implements iEnemyAttacker {
	
	private $oRobot;
	
	public function __construct(EnemyRobot $robot) {
		$this->oRobot = $robot;
	}
	
	public function fireWeapon()
	{
		$this->oRobot->smashWithHands();
	}

	public function driveForward()
	{
		$this->oRobot->walkForward();
	}

	public function assignDriver($driver)
	{
		$this->oRobot->reactToHuman($driver);
	}
}


echo "Tank<br/>";
$tank = new EnemyTank();
$tank->fireWeapon();
$tank->driveForward();
$tank->assignDriver("Fred");

echo "<br/>Robot <br/>";
$robot = new EnemyRobot();
$robot->smashWithHands();
$robot->walkForward();
$robot->reactToHuman("Fred");

//Adapter

echo "<br/> Adapter .. <br/>";
$enemyRobot = new EnemyRobotAttacker($robot);
$enemyRobot->fireWeapon();
$enemyRobot->driveForward();
$enemyRobot->assignDriver("Fred");




?>
