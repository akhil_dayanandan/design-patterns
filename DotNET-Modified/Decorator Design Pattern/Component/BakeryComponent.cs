using System;
using System.Collections.Generic;
using System.Text;

namespace BridgeTechWhizz
{
    public abstract class BakeryComponent
    {
        public abstract string GetName();
        public abstract double GetPrice();
    }
}
