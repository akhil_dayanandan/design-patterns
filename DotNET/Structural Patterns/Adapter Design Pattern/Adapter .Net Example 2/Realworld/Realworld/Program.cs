﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Realworld
{
    class Program
    {
        static void Main(string[] args)
        {
            EnemyTank tx500=new EnemyTank();

            EnemyRobot frank =new EnemyRobot();

            EnemyRobotAttacker red=new EnemyRobotAttacker(frank);


            Console.WriteLine("Enemy tank in action\n--------------------------------------");
            tx500.fireWeapon();
            tx500.driveForward();
            tx500.assignDriver("Tom");




            Console.WriteLine("\n\nEnemy robot in action\n--------------------------------------");
            red.fireWeapon();
            red.driveForward();
            red.assignDriver("Tom");

            Console.ReadKey();
        }


        public interface IEnemyAtacker
        {
            void fireWeapon();
            void driveForward();
            void assignDriver(string driver);

        }

        public class EnemyTank : IEnemyAtacker
        {
            public void fireWeapon()
            {
                Console.WriteLine("Enemy tank fired shots");
            }

            public void driveForward()
            {
                Console.WriteLine("Enemy Tank is moving forward");
            }

            public void assignDriver(string driver)
            {
                Console.WriteLine("Driver {0} is driving the enemy tank ", driver);
            }
        }

        /// <summary>
        /// Adaptee Class
        /// </summary>
        public class EnemyRobot
        {
            public void smashWithHands()
            {
                Console.WriteLine("Enemy robot is causing damage with its hands");
            }

            public void walkForward()
            {
                Console.WriteLine("Enemy robot walks forward");
            }

            public void reactToHuman(string driver)
            {
                Console.WriteLine("Enemy robot tramps on {0}", driver);
            }
        }


        /// <summary>
        /// Adapter Class
        /// </summary>
        public class EnemyRobotAttacker : IEnemyAtacker
        {
            private EnemyRobot theRobot;

            public EnemyRobotAttacker(EnemyRobot newRobot)
            {
                theRobot = newRobot;
            }

            public void fireWeapon()
            {
                theRobot.smashWithHands();
            }

            public void driveForward()
            {
                theRobot.walkForward();
            }

            public void assignDriver(string driver)
            {
                theRobot.reactToHuman(driver);
            }
        }

    }
}
